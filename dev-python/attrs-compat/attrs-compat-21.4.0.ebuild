# Distributed under the terms of the GNU General Public License v2

EAPI=7

PYTHON_COMPAT=( python2_7 )
inherit distutils-r1

DESCRIPTION=""
HOMEPAGE="https://www.attrs.org/ https://pypi.org/project/attrs/"
SRC_URI="https://files.pythonhosted.org/packages/d7/77/ebb15fc26d0f815839ecd897b919ed6d85c050feeb83e100e020df9153d2/attrs-21.4.0.tar.gz -> attrs-21.4.0.tar.gz
"

DEPEND=""
RDEPEND="!<dev-python/attrs-22.1.0 "
IUSE=""
SLOT="0"
LICENSE="MIT"
KEYWORDS="*"
S="${WORKDIR}/attrs-21.4.0"

post_src_install() {
	rm -rf ${D}/usr/bin
}