# Distributed under the terms of the GNU General Public License v2

EAPI=7

PYTHON_COMPAT=( pypy3 python3+ )
inherit distutils-r1

DESCRIPTION=""
HOMEPAGE="https://www.attrs.org/ https://pypi.org/project/attrs/"
SRC_URI="https://files.pythonhosted.org/packages/21/31/3f468da74c7de4fcf9b25591e682856389b3400b4b62f201e65f15ea3e07/attrs-22.2.0.tar.gz -> attrs-22.2.0.tar.gz
"

DEPEND=""
RDEPEND="python_targets_python2_7? ( dev-python/attrs-compat )"
IUSE="python_targets_python2_7"
SLOT="0"
LICENSE="MIT"
KEYWORDS="*"
S="${WORKDIR}/attrs-22.2.0"